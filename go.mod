module hodl.trade/pkg/timeseries

go 1.16

require (
	github.com/influxdata/influxdb-client-go/v2 v2.3.0
	hodl.trade/pkg/journal v0.1.0
)
