package influxdb

import (
	"encoding/csv"
	"fmt"
	"io"
	"strconv"
	"time"

	"hodl.trade/pkg/journal"
	"hodl.trade/pkg/timeseries"
)

var _ timeseries.Flusher = &csvJournal{}

func NewCsvJournal(measurement string, keyTag string, valueField string, tags map[string]string, precision time.Duration, journal journal.Journal) timeseries.Writer {
	// use nearest precision necessary
	tf := time.RFC3339Nano
	if precision >= time.Second {
		tf = time.RFC3339
	}

	return &csvJournal{
		measurement: measurement,
		keyTag:      keyTag,
		valueField:  valueField,
		tags:        tags,
		timeFormat:  tf,

		j: journal,
	}
}

type csvJournal struct {
	// config
	measurement string
	keyTag      string
	valueField  string
	tags        map[string]string
	timeFormat  string

	// state
	j journal.Journal
	f io.WriteCloser
	w *csv.Writer
}

func (t *csvJournal) Write(ts time.Time, values map[string]float64) (err error) {
	if t.j.Next(ts) {
		t.Close()

		if t.f, err = t.j.Rotate(ts); err != nil {
			return
		}
		t.w = csv.NewWriter(t.f)
		t.writeHeader()
	}

	for k, v := range values {
		if err = t.w.Write([]string{ts.Format(t.timeFormat), k, strconv.FormatFloat(v, 'f', -1, 64)}); err != nil {
			return
		}
	}

	return
}

func (t *csvJournal) writeHeader() (err error) {
	fmt.Fprintf(t.f, "#constant measurement,%s\n", t.measurement)
	for k, v := range t.tags {
		fmt.Fprintf(t.f, "#constant tag,%s,%s\n", k, v)
	}

	var tf string
	if t.timeFormat == time.RFC3339 {
		tf = "RFC3339"
	} else if t.timeFormat == time.RFC3339Nano {
		tf = "RFC3339Nano"
	} else {
		tf = t.timeFormat
	}

	return t.w.Write([]string{
		fmt.Sprintf("time|dateTime:%s", tf),
		fmt.Sprintf("%s|tag", t.keyTag),
		fmt.Sprintf("%s|field", t.valueField),
	})
}

func (t *csvJournal) Flush() {
	if t.w == nil {
		return
	}
	t.w.Flush()
}

func (t *csvJournal) Close() error {
	// flush csv writer because file is not aware about its internal bytes buffer
	t.Flush()

	if t.f == nil {
		return nil
	}
	return t.f.Close()
}
