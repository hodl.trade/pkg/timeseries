package influxdb

import (
	"time"

	"github.com/influxdata/influxdb-client-go/v2/api"
	"github.com/influxdata/influxdb-client-go/v2/api/write"
	"hodl.trade/pkg/timeseries"
)

var _ timeseries.Flusher = &writer{}

func NewWriter(measurement string, keyTag string, valueField string, tags map[string]string, db api.WriteAPI) timeseries.Writer {
	return &writer{
		measurement: measurement,
		keyTag:      keyTag,
		valueField:  valueField,
		tags:        tags,

		db: db,
	}
}

type writer struct {
	measurement string
	keyTag      string
	valueField  string
	tags        map[string]string

	db api.WriteAPI
}

func (t *writer) Write(ts time.Time, values map[string]float64) (err error) {
	for k, v := range values {
		p := write.NewPointWithMeasurement(t.measurement)
		p.AddField(t.valueField, v)
		p.SetTime(ts)
		for k, v := range t.tags {
			p.AddTag(k, v)
		}
		p.AddTag(t.keyTag, k)
		t.db.WritePoint(p)
	}

	return
}

func (t *writer) Flush() {
	t.db.Flush()
}

func (t *writer) Close() error {
	t.db.Flush()
	return nil
}
