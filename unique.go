package timeseries

import "time"

func UniqueWriter(writer Writer) Writer {
	return &uniqueWriter{writer: writer}
}

type uniqueWriter struct {
	writer   Writer
	previous map[string]float64
}

func (t *uniqueWriter) Write(ts time.Time, values map[string]float64) (err error) {
	if t.previous == nil {
		t.previous = make(map[string]float64, len(values))
	}

	changes := make(map[string]float64, len(values))
	for k, v := range values {
		if p, ok := t.previous[k]; p == v && ok {
			continue
		}
		t.previous[k] = v
		changes[k] = v
	}

	if len(changes) == 0 {
		return nil
	}

	return t.writer.Write(ts, changes)
}

func (t *uniqueWriter) Close() error {
	return t.writer.Close()
}
