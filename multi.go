package timeseries

import "time"

func MultiWriter(writers ...Writer) Writer {
	return &multiWriter{writers: writers}
}

type multiWriter struct {
	writers []Writer
}

func (t *multiWriter) Write(ts time.Time, values map[string]float64) (err error) {
	for _, w := range t.writers {
		if err = w.Write(ts, values); err != nil {
			return
		}
	}
	return nil
}

func (t *multiWriter) Close() (err error) {
	for _, w := range t.writers {
		if werr := w.Close(); werr != nil {
			err = werr
		}
	}
	return
}
