package timeseries

import (
	"io"
	"time"
)

type Writer interface {
	io.Closer
	Write(time.Time, map[string]float64) error
}

type Flusher interface {
	Flush()
}

// Flusher flushes writer after every write
func AlwaysFlush(w Writer) Writer {
	if f, ok := w.(Flusher); ok {
		return &flush{w, f}
	}
	return w
}

type flush struct {
	w Writer
	f Flusher
}

func (t *flush) Write(ts time.Time, values map[string]float64) (err error) {
	if err = t.w.Write(ts, values); err != nil {
		return
	}
	t.f.Flush()
	return
}

func (t *flush) Close() error {
	t.f.Flush()
	return t.w.Close()
}
